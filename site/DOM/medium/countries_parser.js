function get_countries() {

    fetch('https://raw.githubusercontent.com/samayo/country-json/master/src/country-by-name.json', {method:'get'})
        .then(res => res.json())
        .then((res) => {
            for (let country of res)
            {
                let sel = document.getElementById('countries_list');
                let opt = document.createElement('option');
                opt.appendChild( document.createTextNode(country.country) );
                opt.value = country.country;
                sel.appendChild(opt);
            }
        });
}