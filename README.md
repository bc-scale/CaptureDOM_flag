# CaptureDOM
CaptureDom is a Capture the Flag realised to obtain several aims:
1. To compare different types of XSS vulnerability detector
2. To Understand the XSS Vulnerability 

## Getting start
Start the docker-compose 
```shell
### on Linux/macOS/Windows ###
docker-compose up --build
```
After the docker-compose is started go on:
* http://localhost:8001

Now you can see the CaptureDOM home



